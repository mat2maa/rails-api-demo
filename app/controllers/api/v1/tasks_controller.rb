class Api::V1::TasksController < ApplicationController

  before_filter :authenticate_user_from_token!

  def show
    @task = Task.find(params[:id])
      render status: 200, json: @task, root: true
  end

  def create
    @task = Task.new(secure_params)

    if @task.save
      render status: 200, json: @task, root: true
    else
      render status: 400, json: @task.errors
    end
  end

  private

  def secure_params
    params.require(:task).permit!
  end

end
