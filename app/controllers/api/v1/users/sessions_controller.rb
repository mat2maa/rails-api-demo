class Api::V1::Users::SessionsController < Devise::SessionsController

  skip_before_filter :verify_signed_out_user, only: :destroy

  def create
    email = params[:user][:email]
    password = params[:user][:password]

    @user = User.find_by_email(email.downcase)
    @user.ensure_authentication_token

    if @user.valid_password?(password)
      render status: 200,
             json: {
                 token: @user.authentication_token
             }
    else
      render status: 401,
             json: {
                 message: 'Invalid password.'
             }
    end
  end

  def destroy
    if request.headers['HTTP_X_USER_EMAIL']
      @user = User.find_by_email(request.headers['HTTP_X_USER_EMAIL'])
      @token = request.headers['HTTP_X_USER_TOKEN']
    else
      render status: 400,
             json: {
                 message: 'The request must contain the user email.'
             }
      return
    end

    if @user.nil?
      render status: 401,
             json: {
                 message: 'Invalid email address.'
             }
    else
      if Devise.secure_compare(@user.authentication_token, @token)
        @user.authentication_token = nil
        @user.save
        render status: 200,
               json: {
                   user: @user,
                   message: "Successfully destroyed authentication token for user #{params[:email]}"
               }
      else
        render status: 401,
               json: {
                   message: 'Invalid authentication token'
               }
      end
    end
  end
end