class Api::V1::Users::RegistrationsController < Devise::RegistrationsController

  def create
    email = params[:user][:email]
    password = params[:user][:password]
    password_confirmation = params[:user][:password_confirmation]

    @user = User.new(
        email: email,
        password: password,
        password_confirmation: password_confirmation
    )

    if @user.save
      render status: 200,
             json: {
                 token: @user.authentication_token
             }
    else
      render status: 400,
             json: {
                 errors: @user.errors
             }
    end
  end

end