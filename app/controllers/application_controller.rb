class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  def authenticate_user_from_token!
    if request.headers['HTTP_X_USER_EMAIL'] && request.headers['HTTP_X_USER_TOKEN']
      user_email = request.headers['HTTP_X_USER_EMAIL'].presence
      user       = user_email && User.find_by_email(user_email)

      if user && Devise.secure_compare(user.authentication_token, request.headers['HTTP_X_USER_TOKEN'])
        sign_in user, store: false
      else
        render status: 401, json: { errors: 'You have not provided a correct authorization token' }
      end
    else
      render status: 401, json: { errors: 'You have not provided a correct authorization token' }
    end
  end

end
