require 'rails_helper'
require 'faker'

RSpec.describe Api::V1::Users::RegistrationsController do
  include Devise::TestHelpers

  before :each do
    @request.env["devise.mapping"] = Devise.mappings[:api_v1_user]
    @email = Faker::Internet.email
    @password = Faker::Internet.password
  end

  describe "POST #create" do
    before(:each) { post :create, {
        user: {
            email: @email,
            password: @password,
            password_confirmation: @password
        }
    }, format: :json }

    it "returns the authentication token to the user" do
      expect(JSON.parse(response.body)).to have_key('token')
    end

    it { should respond_with 200 }
  end
end