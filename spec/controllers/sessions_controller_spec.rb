require 'rails_helper'
require 'faker'

RSpec.describe Api::V1::Users::SessionsController do
  include Devise::TestHelpers

  before :each do
    @request.env["devise.mapping"] = Devise.mappings[:api_v1_user]
  end

  describe "POST #create" do
    context "with valid params" do
      let(:user) { FactoryGirl.create(:user) }
      before(:each) { post :create, { user: { email: user.email, password: user.password } }, format: :json }

      it "returns the authentication token to the user" do
        expect(JSON.parse(response.body)).to have_key('token')
      end

      it { should respond_with 200 }
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      request.headers['X-USER-EMAIL'] = "#{user.email}"
      request.headers['X-USER-TOKEN'] = "#{user.authentication_token}"
    end

    let(:user) { FactoryGirl.create(:user) }
    before(:each) { delete :destroy, format: :json }

    it "destroys the user's authentication token" do
      expect(JSON.parse(response.body)).to have_key('user')
    end

    it { should respond_with 200 }
  end
end