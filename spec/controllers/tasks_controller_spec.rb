require 'rails_helper'
require 'faker'

RSpec.describe Api::V1::TasksController, :type => :controller do
  include Devise::TestHelpers

  describe "without a valid token", no_token: true do
    before(:each) { request.headers['X-USER-TOKEN'] = nil }

    describe "GET #show" do
      before(:each) { get :show, id: 0, format: :json }

      it { should respond_with 401 }
    end

    describe "POST #create" do
      before(:each) { post :create, format: :json }

      it { should respond_with 401 }
    end
  end

  describe "with a valid token", no_token: true do
    let(:user) { FactoryGirl.create(:user) }

    before(:each) do
      request.headers['X-USER-EMAIL'] = "#{user.email}"
      request.headers['X-USER-TOKEN'] = "#{user.authentication_token}"
    end

    describe "GET #show" do
      let(:task) { FactoryGirl.create(:task) }
      before(:each) { get :show, id: task.id, format: :json }

      it "returns the task" do
        expect(JSON.parse(response.body)).to have_key('task')
      end

      it { should respond_with 200 }
    end

    describe "POST #create" do
      before(:each) { post :create, { task: { name: Faker::Lorem.sentence, done: false, user_id: user.id } }, format: :json }

      it "returns the task" do
        expect(JSON.parse(response.body)).to have_key('task')
      end

      it { should respond_with 200 }
    end
  end

end
