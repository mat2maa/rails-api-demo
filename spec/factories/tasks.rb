require 'faker'

FactoryGirl.define do
  factory :task do |f|
    f.name { Faker::Lorem.sentence }
    f.done { false }
  end
end